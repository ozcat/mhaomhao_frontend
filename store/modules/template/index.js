import { make } from 'vuex-pathify'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'
const state = {
  isLoading: false
}

export default {
  namespaced: true,
  state,
  mutations: {
    ...make.mutations(state),
    ...mutations
  },
  actions: {
    ...make.actions(state),
    ...actions
  },
  getters: {
    ...make.getters(state),
    ...getters
  }
}
