import Vuex from 'vuex'
import pathify from './pathify'
import modules from './modules'
const createStore = () => {
  return new Vuex.Store({
    plugins: [ pathify.plugin ],
    modules: {
      ...modules
    },
    actions: {
      nuxtServerInit (context, value) {
      }
    }

  })
}

export default createStore
