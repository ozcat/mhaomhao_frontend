export default class User {
  static create (user) {
    return {
      firstName: (user && user.firstName) || '',
      lastName: (user && user.lastName) || ''
    }
  }
}
