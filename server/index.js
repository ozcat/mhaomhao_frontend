const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const session = require('express-session')
const FileStore = require('session-file-store')(session)
app.use((req, res, next) => {
  Object.setPrototypeOf(req, app.request)
  Object.setPrototypeOf(res, app.response)
  req.res = res
  res.req = req
  next()
})

// Body Parser Setting
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Sessions to create `req.session`
app.use(session({
  store: new FileStore({
    path: './server/sessions'
  }),
  secret: 'super-secret-key',
  resave: false,
  saveUninitialized: false,
  cookie: { maxAge: 60000 }
}))
// Use Routes
app.use(require('./routes'))

module.exports = {
  path: '/api',
  handler: app
}
