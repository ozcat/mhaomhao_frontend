const axios = require('axios')
// const baseUrl = 'https://api.github.com/user'
const instance = axios.create()
const request = (req, res) => {
  instance.interceptors.response.use((response) => {
    return response
  }, (error) => {
    if (error.response.status === 401) {
      return request(req, res)
        .get('https://api.github.com/')
        .then((response) => {
          console.log('success response')
          return response
        }).catch((error) => {
          console.log('error')
          return Promise.reject(error)
        })
    }
    return Promise.reject(error)
  })
  return instance
}
module.exports = request
