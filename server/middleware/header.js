const header = (req, res, next) => {
  if (req.session.token) {
    req.apiHeader = {
      'Content-Type': 'application/json',
      'Cache-Control': 'no-cache',
      Authorization: `${req.session.token.token_type} ${req.session.token.access_token}`
    }
    next()
  } else {
    res.status(401).send({ status: 'Unauthorized' })
  }
}
module.exports = header
