const baseUrl = 'http://206.189.87.113:3000/'

module.exports = {
  loginApi: `${baseUrl}dialog/token`,
  registerApi: `${baseUrl}auth/register`,
  sizesApi: `${baseUrl}sizes`,
  materialsApi: `${baseUrl}materials`,
  bordersApi: `${baseUrl}borders`
}
