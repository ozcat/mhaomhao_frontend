const express = require('express')
const router = express.Router()
const request = require('../service')
const { sizesApi, materialsApi, bordersApi } = require('../configs/constants')
router.get('/print/sizes', (req, res) => {
  request(req, res).get(sizesApi)
    .then((response) => {
      const { data } = response
      res.json(data)
    }).catch((error) => {
      const { response: errorResponse } = error || {}
      const { message: errorMessage } = error
      if (errorResponse) {
        const { status, data } = errorResponse
        res.status(status).send(data)
      } else {
        res.status(500).send(errorMessage)
      }
    })
})
router.get('/print/materials', (req, res) => {
  request(req, res).get(materialsApi)
    .then((response) => {
      const { data } = response
      res.json(data)
    }).catch((error) => {
      const { response: errorResponse } = error || {}
      const { message: errorMessage } = error
      if (errorResponse) {
        const { status, data } = errorResponse
        res.status(status).send(data)
      } else {
        res.status(500).send(errorMessage)
      }
    })
})
router.get('/print/borders', (req, res) => {
  request(req, res).get(bordersApi)
    .then((response) => {
      const { data } = response
      res.json(data)
    }).catch((error) => {
      const { response: errorResponse } = error || {}
      const { message: errorMessage } = error
      if (errorResponse) {
        const { status, data } = errorResponse
        res.status(status).send(data)
      } else {
        res.status(500).send(errorMessage)
      }
    })
})
module.exports = router
