const express = require('express')
const router = express.Router()
const request = require('../service')
const headerMiddleWare = require('../middleware/header')
router.get('/test', headerMiddleWare, (req, res) => {
  request(req, res).get('https://api.github.com')
    .then((response) => {
      return res.json({ hello: response.data })
    }).catch((error) => {
      console.log(error)
      return res.status(401).json({ test: '333' })
    })
})
module.exports = router
