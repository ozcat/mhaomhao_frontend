const express = require('express')
const qs = require('qs')
const router = express.Router()
const request = require('../service')
const { loginApi, registerApi } = require('../configs/constants')
router.post('/login', (req, res) => {
  const { username, password } = req.body
  request(req, res).post(loginApi,
    qs.stringify({
      username,
      password,
      grant_type: 'password',
      client_id: 'mhaomhaoweb',
      client_secret: 'hwioteam'
    }), {
      header: {
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cache-Control': 'no-cache'
      }
    }
  ).then((response) => {
    const { data } = response
    req.session.token = data
    req.session.cookie.maxAge = data.expires_in * 1000
    res.status(200).json({
      success: true
    })
  }).catch((error) => {
    const { response: errorResponse } = error || {}
    const { message: errorMessage } = error
    if (errorResponse) {
      const { status, data } = errorResponse
      res.status(status).send(data)
    } else {
      res.status(500).send(errorMessage)
    }
  })
})

router.post('/logout', (req, res) => {
  delete req.session.token
  res.json({ success: true })
})

router.post('/register', (req, res) => {
  const { username, password } = req.body
  request(req, res).post(registerApi,
    qs.stringify({
      username,
      password
    }), {
      header: {
        'Accept-Encoding': 'gzip, deflate',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Cache-Control': 'no-cache'
      }
    }
  ).then((response) => {
    const { data } = response
    res.json(data)
  }).catch((error) => {
    const { response: errorResponse } = error || {}
    const { message: errorMessage } = error
    if (errorResponse) {
      const { status, data } = errorResponse
      res.status(status).send(data)
    } else {
      res.status(500).send(errorMessage)
    }
  })
})
module.exports = router
