const express = require('express')
const router = express.Router()
const auth = require('./auth')
const print = require('./print')
router.use(auth)
router.use(print)
module.exports = router
