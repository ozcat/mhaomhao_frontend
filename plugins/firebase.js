import firebase from 'firebase/app'
import 'firebase/firestore'

if (!firebase.apps.length) {
  const config = {
    apiKey: 'AIzaSyAn6tIFRWce5KbGCFjyeP2riAE5yFKvg5A',
    authDomain: 'mhaomhao-b875a.firebaseapp.com',
    databaseURL: 'https://mhaomhao-b875a.firebaseio.com',
    projectId: 'mhaomhao-b875a',
    storageBucket: 'mhaomhao-b875a.appspot.com',
    messagingSenderId: '849273188642'
  }

  firebase.initializeApp(config)
  firebase.firestore().settings({timestampsInSnapshots: true})
}

const fireStoreDb = firebase.firestore()

export {fireStoreDb}
