import fontawesome from '@fortawesome/fontawesome'
import { faFacebook, faInstagram, faTwitter, faYoutube } from '@fortawesome/free-brands-svg-icons'

fontawesome.config = {
  autoAddCss: false
}

fontawesome.library.add(faFacebook, faInstagram, faTwitter, faYoutube)
