module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Mhao Mhao',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Mhao Mhao Auction System' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Prompt:300,400,500,600,700|Material+Icons' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
  ** CSS
  */
  css: [
    'semantic-ui-css/semantic.min.css',
    '~/css/style.less'
  ],
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    vendor: ['vee-validate'],
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      config.module.rules.push({
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
        loader: 'url-loader'
      })
    },
    postcss: {
      plugins: {
        'postcss-custom-properties': false
      }
    }
  },
  // serverMiddleware: [
  //   '~/server'
  // ],
  plugins: [
    '~/plugins/firebase.js',
    {src: '~plugins/vee-validate.js', ssr: true},
    '~/plugins/i18n.js',
    '~/plugins/font-awesome'
  ],
  router: {
    middleware: 'i18n'
  },
  modules: [
    'semantic-ui-vue/nuxt',
    '@nuxtjs/font-awesome'
  ]
}
