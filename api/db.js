import {fireStoreDb} from '@/plugins/firebase'
export const userCollection = fireStoreDb.collection('user')
export const auctionCollection = fireStoreDb.collection('auction')
export const auctionTransactionCollection = fireStoreDb.collection('auctionTransaction')
export const mediaCollection = fireStoreDb.collection('media')
export const paymentCredentialCollection = fireStoreDb.collection('paymentCredential')
export const paymentTransactionCollection = fireStoreDb.collection('paymentTransaction')
export const roleCollection = fireStoreDb.collection('role')
export const shopCollection = fireStoreDb.collection('shop')
